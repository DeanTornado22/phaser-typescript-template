import "phaser";
import Preloader from "./scenes/Preloader";

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 200 },
      debug: true,
    },
  },
  //   scale: {
  //     width: 1600,
  //     height: 900,
  //   },
  scene: [Preloader],
};

new Phaser.Game(config);
